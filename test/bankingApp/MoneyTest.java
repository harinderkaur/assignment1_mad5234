package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Currency;
import bankingApp.Money;

public class MoneyTest {
	protected Currency CAD, HKD, NOK, EUR;
	protected Money CAD100, EUR10, CAD200, EUR20, CAD0, EUR0, CADnegative100, CAD152;
	
	@Before
	public void setUp() throws Exception {
		// setup sample currencies
		CAD = new Currency("CAD", 0.75);
		HKD = new Currency("HKD", 0.13);
		EUR = new Currency("EUR", 1.14);
		
		// setup sample money amounts
		CAD100 = new Money(100, CAD);
		
		EUR10 = new Money(10, EUR);
		CAD200 = new Money(200, CAD);
		EUR20 = new Money(20, EUR);
		CAD0 = new Money(0, CAD);
		EUR0 = new Money(0, EUR);
		CADnegative100 = new Money(-100, CAD);
		
		CAD152 = new Money(15.2, CAD);
	}

	@Test
	public void testGetAmount() {
		
		double testAmount = 0.0;
		testAmount = EUR10.getAmount();
		assertEquals(10.00,testAmount,0.001);
	}

	@Test
	public void testGetCurrency() {
		
		Currency currency = new Currency("CAD",0.75);
		Currency cur = CAD200.getCurrency();
		assertEquals(cur.getName(),currency.getName());
	}

	@Test
	public void testToString() {

		String output = "";
		output = EUR20.toString();
		//System.out.print(output);
		assertEquals("20.0 EUR",output);

	}

	@Test
	public void testGetUniversalValue() {
		double universalValue = 0.0;
		universalValue = EUR10.getUniversalValue();
		assertEquals(11.4,universalValue,0.001);
	}

	@Test
	public void testEqualsMoney() {
		boolean test = false;
		test = EUR10.equals(CAD152);
		assertEquals(true, test);
	}

	@Test
	public void testAdd() {

		Money output = EUR10.add(CAD200);
		//System.out.print(output.getAmount());
		assertEquals(141.58, output.getAmount(),0.001);
		/*
		 * CAN 200 = EUR 131.58
		 * 200 * 0.75 = 150 / 1.14 = 131.58  
		 * EUR 10 + 131.58 = 141.58
		 */
		
		
		Money output2 = CAD152.add(CAD200);
		assertEquals(215.20, output2.getAmount(),0.001);
		
	}
	

	@Test
	public void testSubtract() {
		Money output = EUR10.subtract(CAD200);
		//System.out.print(output.getAmount());
		assertEquals(-121.58, output.getAmount() ,0.001);
		/*
		 * CAN 200 = EUR 131.58
		 * 200 * 0.75 = 150 / 1.14 = 131.58  
		 * EUR 10 - 131.58 = 141.58
		 */
	
		Money output2 = CAD152.subtract(CAD200);
		assertEquals(-184.80, output2.getAmount(),0.001);
		
	}

	@Test
	public void testIsZero() {
		boolean output = false;
		output = CAD0.isZero();
		assertEquals(true, output);
	}

	@Test
	public void testNegate() {
		Money negate = CAD200.negate();
		assertEquals(-200.00, negate.getAmount(),0.001);
	}

	@Test
	public void testCompareTo() {
		//int compareTo(Object other)
		int value = 0;
		value = CAD152.compareTo(CAD200);
		assertEquals(-138,value);
	}
}
