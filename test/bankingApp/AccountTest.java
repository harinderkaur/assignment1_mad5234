package bankingApp;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Account;
import bankingApp.AccountDoesNotExistException;
import bankingApp.Bank;
import bankingApp.Currency;
import bankingApp.Money;

import static org.junit.Assert.*;

public class AccountTest {
	protected Currency CAD, HKD;
	protected Bank TD;
	protected Bank HSBC;
	protected Bank RBC;
	protected Account testAccount;
	
	@Before
	public void setUp() throws Exception {
		
		// setup test currencies
		CAD = new Currency("CAD", 0.75);
		RBC = new Bank("Royal Bank of Canada", CAD);
		
		// setup test accounts
		RBC.openAccount("Peter");
		testAccount = new Account("Albert", CAD);
		testAccount.deposit(new Money(100, CAD));

		// setup an initial deposit
		RBC.deposit("Peter", new Money(500, CAD));
	}

	@Test 
	public void testAddWithdraw() {
		// Something to consider - can you withdraw in a different currency?

		Money withdrawMoney = new Money(50, CAD);
				
		testAccount.withdraw(withdrawMoney);
	
		try {
			RBC.withdraw("Peter", new Money(200, CAD));
		} catch (AccountDoesNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		double afterWithdraw = 0.0;
		try {
			afterWithdraw = RBC.getBalance("Peter");
		} catch (AccountDoesNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print(afterWithdraw);
				
		assertEquals(50.00,afterWithdraw,0.001);
		
	}
	
	@Test
	public void testGetBalance() {
		double balance = 0.0;
		try {
			balance = RBC.getBalance("Peter");
		} catch (AccountDoesNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print(balance);
				
		assertEquals(50.00,balance,0.001);
		
	}
}
